#!/bin/bash

hosts=$*
query="select pg_xlog_location_diff(pg_last_xlog_replay_location(),'0/0')::bigint"

for addr in $hosts; do 
   echo -e "$addr:$(psql -qAtX -h $addr -U postgres -c "$query")";
done |sort -t: -nk2 |tail -n 1 |cut -d: -f1 |tr -d '\n'
