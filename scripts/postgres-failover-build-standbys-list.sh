#!/bin/bash

n_hosts=$#
master=$1

for (( i=2 ; i <= n_hosts; i++ ))
do
  echo "$2"
  shift
done |grep -v $master
