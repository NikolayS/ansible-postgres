#### pgGalaxy - Ansible for PostgreSQL and others.

##### Important things.
Every day we working with many customers which have different environment (filepaths, package versions, configuration files, etc). On the other hand we trying to minimize the number of playbooks and don't duplicate playbooks for each customers. For this reason we use separate files with customer's settings and variables. We have invetory files with customer's hosts and variables files. 
- Customer inventory. Every customer has own inventory file where placed information about hosts and other connection information.
- Customer variables. Variables file named to according with customer name and placed in defaults/ directory. At ansible-playbook startup we specify customer inventory which has special variable with customer name. And if customer variables file exists, it's content included into playbook and override default variables.
- Management node. In case of failover in streaming replication cluster, management node must have installed psql and access to the postgres services on standbys. It need for determining which host become a new master.

#### Available playbooks
##### PostgreSQL
- setup-pgdg-repo-apt.yml - setup postgresql official repo for apt package manager (Debian family).
- setup-pgdg-repo-yum.yml - setup postgresql official repo for yum package manager (RHEL family).
- postgres-install-apt.yml - install postgresql related packages on Debian/Ubuntu. Note, after installation, Debian/Ubuntu built-in scripts will init and run default postgres instance. 
- postgres-install-yum.yml - install postgresql related packages on RHEL/Scientific/CentOS. 
- postgres-sync-hba.conf - configure pg_hba.conf on hosts (reload may be specified on-demand). All authentication rules should be defined in templates/pg_hba.conf.j2. By default, playbook don't reload postgresql service after editing conf, if ypu need to reload service, run ansible-playbook with "-e do_reload=yes".
- postgres-initdb.yml - initialize postgresql instance. Required on RHEL family distros after fresh postgresql install.
- postgres-service-mgmt.yml - start/stop/restart/reload postgresql service via pg_ctl. Used with following variables: do_start, do_stop, do_restart, do_reload which are disabled by default. For performs exact actions run the playbook with "-e <var>=yes".

##### PostgreSQL Streaming Replication
- postgres-configure-master.yml - configure master for streaming replication. This playbook edit postgresql.conf and enables streaming replication options. By default, playbook don't restart postgresql service. If service shoul be restart run ansible-playbook with "-e do_restart=yes"
- postgres-configure-slaves.yml - configure slaves for streaming replication. This playbook creates streaming replication hot-standby. It stops postgresql service running on the host and remove service's data directory. After clean up, it connects to the master, do basebackup and when it's done start a postgresql service in standby mode. If node already in cluster, it will not reinitialized and skipped.

##### PostgreSQL Switchover/Failover
- postgres-switchover.yml - perform switchover in live healthy cluster. Current master server and postgresql must be alive and running, otherwise switchover will be failed. By default, when new master will be promoted, old master stayed down. If old master should be started as standby, run ansible-playbook with "-e start_old_master=yes".
- postgres-failover.yml - perform failover to one of the standby. Use this playbook in case when master service or host is down. This playbook check amount of replayedWAL between all standbys and select appropriate host as new master. Other hosts will reconfigured for new master and restarted. 

##### Pgpool-II
- pgpool-install-debian.yml - install the pgpool-II related packages on Debian/Ubuntu. This playbook installs pgpool package. Pgpool service will start automatically by Debian/Ubuntu.
- pgpool-configure.yml - configure pgpool-II. This playbook configures pgpool main conf and PCP auth configuration.

##### Pgbouncer
- pgbouncer-install-debian.yml - the playbook installs the pgbouncer package. The pgbouncer service sill start automatically after install by Debian/Ubuntu.
- pgbouncer-configure.yml - pgbouncer initial configuration (autostart, nofile limits) and userlist.txt deploy.
- pgbouncer-pause.yml - pause all pgbouncers, all connections will be graceful stopped without client disconnections, until resume issued.
- pgbouncer-resume.yml - resume all pgbouncers, all connections will continues their work as usual.

#### Amazon EC2
Setup environment before using AWS playbooks:
```
$ sudo yum install python-boto python-pip
$ sudo pip install awscli
$ awscli --version
$ awscli configure
Access Key ID:      REPLACED
Secret Access Key:  REPLACED
Region:             REPLACED
$ awscli ec2 describe-instances
```
- Check that you have key pair. It will be associated with instances (see "EC2 Console -> Network & Security -> Key pairs").

- playbooks/aws-ec2-provision-standby.yml - Provision new Amazon EC2 instance, setup packages and connect instance into streaming cluster.
- playbooks/aws-ec2-setup-volume-autoexpand.yml - Setup volume autoexpand script and cronjob. Autoexpand script check used space on specified filesystem, if used space exceeds threshold, create and attach new volume, add it to LVM and resize filesystem. See playbook variables in aws.yml.

#### WAL-E
- setup-wale-yum.yml - setup WAL-E, configure backup to s3.
